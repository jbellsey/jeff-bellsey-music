
// TODO:
//  • image management (imagemin)
//

//===================================
//====== CONFIGURATION SECTION ======
//===================================

// gulp configs
var GC = {

    // the main application folders
    //
    sourceFolder: './app',
    destFolder:   './dist',

    // javascript files
    //
    JS: {
        folder:             '/js/',
        inputFiles:         '/js/master.js',    // the browserify master input
        appScriptsFinal:    'app.min.js'
    },

    // styles
    //
    LESS: {
        inputFiles:         '/css/music.less',
        watchFiles:         '/css/*.less'
    }
};


//===================================
//======== END CONFIGURATION ========
//===================================

//-----------------------

// all gulp plugins are lazy-loaded onto the "plugins" object.
// see package.json for the list, and "gulp-load-plugins" for the technique.
//
var gulp    = require('gulp'),
    plugins = require('gulp-load-plugins')({
        // glob of files to treat as gulp plugins
        pattern: [
            'gulp-*',
            'browser-sync',
            'run-sequence',
            'browserify']
    });

//-----------------------

// TASK: jshint
//  RUN AFTER: browserify (it hints the output file in /app)
//  Note: this doesn't do anything with the minified prod scripts.
//
gulp.task('jshint', function() {
    return gulp.src(GC.sourceFolder + GC.JS.appScriptsFinal)
        .pipe(plugins.jshint())
        .pipe(plugins.jshint.reporter('jshint-stylish'));
});


//-----------------------
//
// TASK: styles
//  EFFECTS: compiles all LESS files in the app
//          generates "app.css" in source dir (uncompressed)
//          and "app.css" in dest dir (compressed)
//
gulp.task('styles', function() {

    // this one file is sufficient; it imports the rest of them
    //
    var cssFiles = GC.sourceFolder + GC.LESS.inputFiles;

    return gulp.src(cssFiles)

        .pipe(plugins.less())

        // autoprefix for cross-browser goodness
        .pipe(plugins.autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))

        // store an uncompressed version in the source directory as "app.css"
        .pipe(plugins.rename('css/app.css'))
        .pipe(gulp.dest(GC.sourceFolder))

        // and a minified version in the destination directory as "app.min.css"
        .pipe(plugins.minifyCss())
        .pipe(gulp.dest(GC.destFolder))

        .pipe(plugins.browserSync.reload({stream:true}));
});

//-----------------------
//
// TASK: copy-directory
//  EFFECTS: deep-copy an entire directory from SOURCE to DEST
//
//  see:  https://github.com/klaascuvelier/gulp-copy
//
gulp.task('copy-directory', function() {

    var copyAsIs = [
        './app/i/**',
        './app/font/**',
        './app/mp3/**',
        './app/.htaccess',
        './app/browserconfig.xml'
    ];

    return gulp.src(copyAsIs, {base:GC.sourceFolder})
        .pipe(gulp.dest(GC.destFolder));

});

//-----------------------
//
// TASK: clean
//  EFFECTS: deletes the entire output folder
//
// see:  https://github.com/robrich/gulp-rimraf
//  (although we're using rimraf directly, not gulp-rimraf)
//
gulp.task('clean', function (cb) {
    return require('rimraf')(GC.destFolder, cb);
});


//-----------------------
//
// TASK: browserify, browserify-prod
//
//  EFFECTS: builds app.js (in the appropriate directory)
//
// see:  http://www.browsersync.io/docs/gulp/
//
//

    var browserifier = function(isForProduction) {

        var source      = require('vinyl-source-stream'),
            streamify   = require('gulp-streamify'),

            inputFiles  = GC.sourceFolder + GC.JS.inputFiles,
            outputFolder= (isForProduction ? GC.destFolder : GC.sourceFolder) + GC.JS.folder,
            outputFile  = GC.JS.appScriptsFinal;

        return plugins.browserify(inputFiles)
            .bundle()
            .pipe(source(outputFile))

            // strip debugging code (console.log)
            .pipe(isForProduction ? streamify(plugins.stripDebug()) : plugins.util.noop())

            // uglify
            .pipe(isForProduction ? streamify(plugins.uglify()) : plugins.util.noop())
            .pipe(gulp.dest(outputFolder))

            // in development, allow for browser synching
            .pipe(isForProduction ? plugins.util.noop() : plugins.browserSync.reload({stream:true}));

    };

// this generates "app.min.js" in the SOURCE folder
//
gulp.task('browserify', function() {

    return browserifier(false);
});

// this is for building the production file.
// it generates "app.min.js" in the OUTPUT folder
//
gulp.task('browserify-prod', function() {

    return browserifier(true);
});


//-----------------------
//
// TASK: copy-index
//  EFFECTS: collapses <build> groups in index file
//          copies minified index.html to dest
//
// see:  https://github.com/julien/gulp-processhtml   (replaces BUILD groups in the HTML file)
//
gulp.task('copy-index', function() {

    var mainHTML = GC.sourceFolder + '/index.html';

    return gulp.src(mainHTML)
        .pipe(plugins.minifyHtml())
        .pipe(gulp.dest(GC.destFolder));
});


//-----------------------
//
// TASK: build
//
// yeah, this is the main task
//

gulp.task('build', function(callback) {

    // conspicuously absent: jshint.
    // do that during serve, not build.
    //
    plugins.runSequence(
        'clean',
        ['browserify-prod', 'copy-directory', 'styles'],
        'copy-index',
        callback);
});

gulp.task('build-dev', function(callback) {
    plugins.runSequence(
        'browserify',
        'jshint',
        'styles',
        callback);
});


//-----------------------
//
// TASK: serve
//

// helper task for browser syncing
//
gulp.task('browser-sync', function() {
    plugins.browserSync({
        server: {
            baseDir: GC.sourceFolder
        }
    });
});

gulp.task('serve', ['build-dev', 'browser-sync'], function () {

    gulp.watch(GC.sourceFolder + GC.JS.folder + '**', ['browserify']);
    gulp.watch(GC.sourceFolder + GC.LESS.watchFiles, ['styles']);
});

