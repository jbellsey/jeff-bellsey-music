# Jeff’s Music Site

Just a li’l demo site for showing off some music. If you’re here,
you’re probably more interested in it for the code than the music.
So, let’s get into it!

# Running the code

Download the dependencies:

    npm install
   
Then run the app locally:

    gulp serve
   
Or go through the full build process, which generates a nice minified
package in /dist:

    gulp build
   
# Libraries

None. To say there's no third-party code in here would be a lie, but only
in the "standing on the shoulders of giants" variety. There actually
are no dependencies on any client-side library.

In fact, this project was like a shower after a long hike. Doing everything by
hand is extremely refreshing when you spend your days using other people's 
libraries for everything. No, for this one, everything is hand-rolled: the DOM
manipulation, animation, audio processing, visualizations — everything.

That's not necessarily an ideal way to code all projects. There are lots of 
libraries that are very useful in production code, of course. Still, it's
fun to have a `package.json` with an empty `dependencies` block.

# Compatibility

I spend my days coding to very old browsers. Blackberries, feature phones,
IE7. No joke.

For this project, I gave myself license to only target newer browsers.
This app assumes your browser can handle the full ES5 feature set, 
including modern DOM APIs, canvas, requestAnimationFrame,
the Audio API, etc. 

I have other repos with ES6 and ES7 code. This one was written before I
started running everything through Babel. 

# Audio files

I've included some sample audio files so the repo will run properly. They're
not the actual audios from the site.