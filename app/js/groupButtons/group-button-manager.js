
var GroupButton = require('./group-button'),
    DomReady    = require('../tools/dom-ready');

var groupButtons    = [],
    hilitedIndex    = -1;

// given a DOM elt, find the index of the object in the
// groupButtons array that matches
//
function findButtonIndexByElement(elt) {
    var result = -1;
    groupButtons.forEach(function(btn,ix) {
        if (btn.elt === elt)
            result = ix;
    });
    return result;
}

// click handler for group buttons
//
function groupButtonClick(e) {

    var index = findButtonIndexByElement(e.currentTarget);

    // user clicked on the selected button => noop
    //
    if (index === hilitedIndex)
        return;

    // change the look of the buttons
    //
    groupButtons.forEach(function(btn, ix) {
        if (ix === index)
            btn.turnOn();
        else
            btn.turnOff();
    });

    // coming in!
    //
    hilitedIndex = index;
}

DomReady(function() {

    var groupContentses = document.getElementById('group-contents').querySelectorAll('div');

    // fill an array with custom objects, built from
    // DOM elements. add event listeners while we're at it
    //
    [].forEach.call(document.getElementById('group-header').querySelectorAll('a'), function(btn, ix) {
        btn.addEventListener('click', groupButtonClick);
        groupButtons.push(new GroupButton(btn, groupContentses.item(ix), ix));
    });
});

// no exports.