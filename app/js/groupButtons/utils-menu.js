var DomReady     = require('../tools/dom-ready'),
    GA           = require('../tools/ga'),
    Transitioner = require('../tools/transitioner'),
    Emitter      = require('../particle/emitter');

/*
    a little bit of code to manage the "utilities"
    popup menu
*/

var open = false,
    menuInner, menuRoot,
    trigger,
    groupWrapper;

function keyHandler(e) {
    if (e.keyCode == 27)
        closeUtilitiesMenu();
}
function docClick(e) {
    e.stopPropagation();
    closeUtilitiesMenu();
}

function openUtilitiesMenu() {

    Transitioner.Factory(menuInner).animate({
        animClass: 'utils-open',
        addAtEnd: 'endState-opacity1'
    });

    document.addEventListener('click', docClick);
    document.addEventListener('keydown', keyHandler);
    groupWrapper.classList.add('dim');
    trigger.classList.add('on');
    open = true;
}

function closeUtilitiesMenu() {

    Transitioner.Factory(menuInner).animate({
        animClass: 'utils-close',
        removeAtStart: 'endState-opacity1',
        addAtEnd: 'endState-opacity0'
    });

    document.removeEventListener('click', docClick);
    document.removeEventListener('keydown', keyHandler);
    groupWrapper.classList.remove('dim');
    trigger.classList.remove('on');
    open = false;
}

function toggleUtilitiesMenu(e) {

    e.stopPropagation();
    if (open)
        closeUtilitiesMenu();
    else
        openUtilitiesMenu();
}

function menuItemClick(e) {

    var elt = e.target,
        action = elt.getAttribute('data-action'),
        allowViz;

    // toggle visualizations
    //
    if (action === 'viz') {

        // tell the emitter what we're doing
        allowViz = Emitter.ToggleMasterSwitch();

        // rename the button in our menu
        elt.innerHTML = 'Turn ' + (allowViz ? 'off' : 'on') + ' Visualizations';

        // turn off all the spinny notes
        [].forEach.call(document.querySelectorAll('.notes'), function(elt) {
            elt.style.display = allowViz ? 'block' : 'none';
        });

        // track user request
        GA.event('Options', 'Viz - ' + (allowViz ? 'Reenable' : 'Disable'), 'Visualizations')
    }

    // allow event propagation; the document will handle the click and close the menu
}

DomReady(function() {

    menuRoot = document.getElementById('utilities');
    trigger  = document.getElementById('open-utilities-btn');

    trigger.addEventListener('click', toggleUtilitiesMenu);
    menuInner = menuRoot.querySelector('.inner');

    // intercept clicks on the menu items
    [].forEach.call(menuInner.querySelectorAll('a'), function(elt) {
        elt.addEventListener('click', menuItemClick);
    });

    // we need the master group wrapper, so we can dim it when the menu opens
    groupWrapper = document.getElementById('group-contents');
});