"use strict";

var Transitioner = require('../tools/transitioner'),
    AudioButton = require('../audio/audioButton');

// this template gets installed into each group button
//
var groupButtonTemplate = [
        '<div class="circles">',
            '<span class="rest"><span></span></span>',
            '<span class="notes">',
                '<span></span>',    // these random notes filled in below
                '<span></span>',    // to add more, just add more empty spans
                '<span></span>',
            '</span>',

            // five circles make a musical staff
            '<div><div><div><div><div>', '</div></div></div></div></div>',
        '</div>'
    ].join(''),

    // pick CT random icons from the iconClasses list above.
    // we sort that array randomly, then return a new array
    // of the correct size
    //
    getRandomIcons = function(ct) {

        // the set of notes from which we can draw
        var iconClasses = [
            'icon-dotted',
            'icon-eighth',
            'icon-eighths',
            'icon-quarter',
            'icon-sixteenths'
        ];
        iconClasses.sort(function() { return Math.random() - 0.5; });
        return iconClasses.slice(0,ct);
    };


var GroupButton = function(domElement, contentArea, index) {

    this.elt         = domElement;
    this.state       = 0;
    this.index       = index;
    this.contentArea = contentArea;

    // install the fancy DOM stuff we use to manage the outer staff-circles
    // and the moving elements (rests & musical notes)
    //
    domElement.insertAdjacentHTML('beforeend', groupButtonTemplate);

    //----------
    // managing the animations

    // rotation of the rest
    //
    var restElt = domElement.querySelector('.rest');

    // the inner span gets faded in & out. we need a separate element
    // for the fading, because we access animationEnd events. having
    // multiple animations on the same element won't work
    //
    this.restSubElt = restElt.querySelector('span');

    // rotation classes set up a varied offset
    restElt.classList.add('rest-rotation-' + (index+1));

    //======== TODO: in process !!!

    // prep the animations for the spinning notes that show up
    // when the group is on
    //
    var noteElt         = domElement.querySelector('.notes'),
        singleNoteElts  = noteElt.querySelectorAll('span'),
        singleNoteElt,
        singleNoteInner,
        i = 0,
        ct = singleNoteElts.length,     // we get the note count from the template (above)
        icons = getRandomIcons(ct);     // pull N random note icons for this button

    this.notesElt = noteElt;

    for (; i < ct; ++i) {
        singleNoteElt = singleNoteElts.item(i);

        // attach a class (which defines a note) to this elt.
        // also add the animation classes here
        //
        singleNoteElt.classList.add('fadePulse-' + (i+1));

        // add a new span to hold the note itself, which also gets the rotation animation
        //
        singleNoteElt.insertAdjacentHTML('beforeend', '<span></span>');
        singleNoteInner = singleNoteElt.querySelector('span');
        singleNoteInner.classList.add(icons[i], 'rotate', 'rotate-' + (i+1));
    }
};

// button state: ON means selected, OFF means faded.
// note: the event handler for clicks is installed by
// the groupButtonManager
//
GroupButton.prototype.turnOn = function() {

    // fade in the roving notes
    //
    Transitioner.Factory(this.notesElt).animate({
        animClass: 'fadeIn',
        addAtEnd:  'endState-opacity1'
    });

    // fade out the REST when this button is ON
    //
    Transitioner.Factory(this.restSubElt).animate({
        animClass: 'fadeOut',
        addAtEnd: 'endState-opacity0'
    });

    // bring in the new content area
    //
    Transitioner.Factory(this.contentArea).animate({
        animClass:   'songGroupIn',
        addAtEnd:    'endState-opacity1',
        waitForThese: this.contentArea.children,
        backstopTime: 2500
    });

    this.elt.classList.remove('off');
    this.elt.classList.add('on');
};

GroupButton.prototype.turnOff = function() {

    if (this.elt.classList.contains('on')) {

        // turn off any playing audio
        AudioButton.destroyCurrentAudio();

        // fade in the REST
        Transitioner.Factory(this.restSubElt).animate({
            animClass: 'fadeIn',
            removeAtStart: 'endState-opacity0'
        });

        // fade OUT the contents
        Transitioner.Factory(this.contentArea).animate({
            animClass:   'songGroupOut',
            removeAtEnd: 'endState-opacity1',
            waitForThese: this.contentArea.children,
            backstopTime: 2000
        });

        // and hide the roving notes
        //
        Transitioner.Factory(this.notesElt).animate({
            animClass: 'fadeOut',
            removeAtStart: 'endState-opacity1'
        });
    }

    this.elt.classList.remove('on');
    this.elt.classList.add('off');
};

module.exports = GroupButton;
