var Transitioner   = require('../tools/transitioner'),
    GA             = require('../tools/ga'),
    Visualizations = require('./viz/visualizations'),
    DomReady       = require('../tools/dom-ready'),
    Templates      = require('./audio-templates');


/*
 manages the audio buttons, plus:
 * the audio itself (play, pause, seek)
 * the visualizations triggered by the audio

 reference material:
 http://html5doctor.com/html5-audio-the-state-of-play/
 */


// operations that only apply to the single, globally-managed
// active audio.
//
var ActiveButton = {

    activeAudioButton: null,

    playCurrentAudio: function() {
        var b = this.activeAudioButton;
        if (!b)
            return;

        // switch the icon in the button (play/pause)
        b.setPlayIcon('pause');

        // play the audio
        b.audioEl.play();

        // and start the visualization
        b.viz.audioStart();

        // track event
        if (!b.everPlayed) {
            b.everPlayed = true;
            GA.event('AudioPlay', b.audioID);
        }
        // track replays, but only if the user listened to the
        // whole thing. we don't want to track all the clicks
        // on the pause button, followed by continue
        //
        else if (b.playedThrough) {
            GA.event('AudioREPlay', b.audioID);
        }
    },

    pauseCurrentAudio: function() {
        var b = this.activeAudioButton;
        if (b) {
            b.setPlayIcon('play');
            b.viz.audioStop();
            b.audioEl.pause();
        }
    },

    playbackEnded: function() {
        var b = this.activeAudioButton;
        if (b) {
            b.playedThrough = true;
            this.destroyCurrentAudio();
        }
    },

    destroyCurrentAudio: function() {
        var b = this.activeAudioButton;
        if (!b)
            return;

        var bar = b.el.nextElementSibling,
            activeViz = b.viz;

        this.pauseCurrentAudio();
        this.activeAudioButton = null;

        Transitioner.Factory(bar).animate({
            animClass: 'playBarExit',
            cb: function() {
                bar.parentNode.removeChild(bar);
                activeViz.reset();
            }
        });
    }
};

function friendlyTime(t) {
    var M = Math.floor(t / 60),
        S = Math.floor(t - (M * 60));

    if (S < 10)
        S = "0" + S;
    return M + ':' + S;
}

/**
 * @param el     (audio element)
 * @constructor
 */
var AudioButton = function(el) {
    this.el = el;

    // put in the play icon
    el.insertAdjacentHTML('afterbegin', Templates.playSVG);
    this.setPlayIcon('play');

    this.audioID = el.getAttribute('data-mp3');
    this.everPlayed = false;    // for analytics tracking

    // append audio element, then set it up
    el.insertAdjacentHTML('beforeend',
        Templates.audio.replace('@@@',  this.audioID)
    );
    var a = this.audioEl = el.querySelector('audio');
    a.addEventListener('timeupdate', this.playbarProgress.bind(this));
    a.addEventListener('progress',   this.playbarBuffering.bind(this));
    a.addEventListener('ended',      ActiveButton.playbackEnded.bind(ActiveButton));

    // add in a visualization manager. we've stubbed out the listener for now
    this.viz = Visualizations.Factory(null, el, el.getAttribute('data-viz'));

    // click handler
    el.addEventListener('click', this.click.bind(this));
};
var ABP = AudioButton.prototype;

/**
 * Click handler for audio buttons (not the play bar)
 */
ABP.click = function(e) {

    // clicked the active playing audio
    //
    if (this === ActiveButton.activeAudioButton) {

        if (this.audioEl.paused)
            ActiveButton.playCurrentAudio();
        else
            ActiveButton.pauseCurrentAudio();
    }
    // start a new audio
    else {
        ActiveButton.destroyCurrentAudio();

        // add a new player bar
        this.el.insertAdjacentHTML('afterend', Templates.playBarTemplate);
        this.playBar           = this.el.nextSibling;
        this.playHead          = this.playBar.querySelector('.bar');
        this.playBarBuffer     = this.playBar.querySelector('.buffer');
        this.playBarBackground = this.playBar.querySelector('.barbox');
        this.playBarBackground.addEventListener('click', this.playBarClick.bind(this));
        this.setDisplayTime();

        ActiveButton.activeAudioButton = this;

        // play!
        ActiveButton.playCurrentAudio();
    }
    e.stopPropagation();
    e.preventDefault();
};

// display the audio duration
// may not be available yet, so we poll periodically
//
ABP.setDisplayTime = function() {
    if (this.audioEl.duration)
        this.playBar.querySelector('.time').innerHTML = friendlyTime(this.audioEl.duration);
    else
        setTimeout(ABP.setDisplayTime.bind(this), 250);
};

// the play/pause icon swap. we have to create the
// SVG elements directly, rather than just inserting
// code. firefox doesn't accept innerHTML or appendChild
// otherwise.
//
ABP.setPlayIcon = function(icon) {

    var g = this.el.querySelector('#icon'),     // load the old <g>
        svg = "http://www.w3.org/2000/svg";

    // clean out the old icon
    while (g.firstChild)
        g.removeChild(g.firstChild);

    // build a new SVG structure. recipes are defined in the template structure above
    Templates.icons[icon].forEach(function(obj) {
        var attr, e = document.createElementNS(svg, obj.elt);
        for (attr in obj.attrs) {
            if (obj.attrs.hasOwnProperty(attr))
                e.setAttribute(attr, obj.attrs[attr]);
        }
        g.appendChild(e);
    });
};

// play bar has moved... update the progress bar
//
ABP.playbarProgress = function() {

    // update the progress bar
    if (this.playHead) {
        var pct = 100 * (this.audioEl.currentTime / this.audioEl.duration);
        this.playHead.style.width = pct.toFixed(1) + '%';
    }
};

// audio buffer tracking  (http://mzl.la/1JUwpz2)
//
ABP.playbarBuffering = function() {

    var a = this.audioEl,
        endBuffer,
        duration;

    if (a.buffered && a.buffered.length && a.duration) {
        endBuffer = a.buffered.end(a.buffered.length - 1);
        duration  = a.duration;

        if (duration > 0 && this.playBarBuffer)
            this.playBarBuffer.style.width = (100 * (endBuffer / duration)).toFixed(1) + "%";
    }
};

// playbar click handler (user seek request)
//
ABP.playBarClick = function(e) {

    var a   = this.audioEl,
        r   = this.playBarBackground.getBoundingClientRect(),
        pct = (e.clientX - r.left) / r.width;

    if (a.seekable && a.seekable.length > 0)
        a.currentTime = pct * a.duration;

    e.stopPropagation();
    e.preventDefault();
};


//-----------

DomReady(function() {

    // set up each audio button
    [].forEach.call(document.querySelectorAll('a[data-mp3]'), function (e) {
        new AudioButton(e);
    });
});

// public API. most of this file is private.
//
module.exports = {

    destroyCurrentAudio: function() {
        ActiveButton.destroyCurrentAudio();
    }
};