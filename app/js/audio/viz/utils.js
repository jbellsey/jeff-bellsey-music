
module.exports = {

    // general-use random number generator.
    // required: provide a range (lo-hi)
    // optional: set 'r' to true to force rounding
    //
    rnd: function(lo,hi,r) {
        var x = lo + (hi-lo) * Math.random();
        return r ? Math.round(x) : x;
    },

    // general linear interpolation, with fixed decimal
    lerp: function (a,b,pct,digits) {
        return (a + pct * (b - a)).toFixed(digits);
    },

    // interpolate between two HSL color codes.
    // returns a string suitable for CSS.
    //
    hslInterp: function(c1, c2, pct) {
        return 'hsl(' + lerp(c1.H, c2.H, pct) + ','
        + lerp(c1.S, c2.S, pct) + '%,'
        + lerp(c1.L, c2.L, pct) + '%)';
    }
};
