"use strict";

var VizUtils       = require('./utils'),
    Particle       = require('../../particle/particle'),
    Emitter        = require('../../particle/emitter');

    // general particle-generation variables
var particleLifeSpan = 2000,    // lose 20% every second
    maxParticles     = 1000,
    losePerSecPct    = 1000/particleLifeSpan,   // 1000 is const
    addPerSec        = maxParticles * losePerSecPct,
    rnd              = VizUtils.rnd,
    pi               = Math.PI,
    pi2              = pi * 2,

    canvasWid,
    canvasHt,

    // sphere-particle generation details
    particleRad = 2.5,
    sphereRad = 720,
    sphereCenterX = 0,
    sphereCenterY = 0,
    sphereCenterZ = -3 - sphereRad,

    // the sphere will rotate at this speed (one complete rotation every 1600 frames)
    turnSpeed = pi2 / 1600,
    rgbString = 'rgba(200,200,222,',    // alpha value will be added later

    // projection
    fLen = 550,         // the distance from the viewer to z=0 depth.
    projCenterX = 0,
    projCenterY = 0,
    zMax = fLen - 2,      // particles that get close to the observer are not drawn

    // alpha values will lessen as particles move further back, causing depth-based darkening:
    zeroAlphaDepth = -750,

    randAccelX = 0.0001,
    randAccelY = 0.0001,
    randAccelZ = 0.0001,

    gravity = 0,    // downward float factor. try up to 0.3, or -0.3 for upward


// updated each frame
    status = {
        turnAngle: 0
    };


// bound to particle
//
function drawBall(context, x, y, width, height, lifePercent) {

    var sinAngle = Math.sin(status.turnAngle),
        cosAngle = Math.cos(status.turnAngle),
        p = this.data,
        rotX, rotY, rotZ,
        projX, projY,
        m, outOfView,
        depthAlphaFactor;

    // if the particle is past its "stuck" time, it will begin to move.
    if (++p.age > p.stuckTime) {

        //p.velX += p.accelX + randAccelX*(Math.random()*2 - 1);
        //p.velY += p.accelY + randAccelY*(Math.random()*2 - 1);
        //p.velZ += p.accelZ + randAccelZ*(Math.random()*2 - 1);

        p.x += p.velX;
        p.y += p.velY;
        p.z += p.velZ;
    }

    /*
     We are doing two things here to calculate display coordinates.
     The whole display is being rotated around a vertical axis, so we first calculate rotated coordinates for
     x and z (but the y coordinate will not change).
     Then, we take the new coordinates (rotX, y, rotZ), and project these onto the 2D view plane.
     */
    rotX =  cosAngle * p.x + sinAngle * (p.z - sphereCenterZ);
    //rotY =  cosAngle * p.y + sinAngle * (p.z - sphereCenterZ);
    rotZ = -sinAngle * p.x + cosAngle * (p.z - sphereCenterZ) + sphereCenterZ;
    m = fLen/(fLen - rotZ);

    projX = rotX * m + projCenterX;
    projY = p.y  * m + projCenterY;

    // see if the particle is still within the viewable range.
    outOfView = false; // ((projX > canvasWid)||(projX<0)||(projY<0)||(projY>canvasHt)||(rotZ>zMax));

    if (!outOfView) {
        // depth-dependent darkening
        //depthAlphaFactor = 1 - rotZ/zeroAlphaDepth;
        //depthAlphaFactor = (depthAlphaFactor > 1) ? 1 : ((depthAlphaFactor<0) ? 0 : depthAlphaFactor);

        //if (p.projX - projCenterX > 0 && p.projY - projCenterY < 0) {
            context.fillStyle = rgbString + '1)';   //depthAlphaFactor * p.alpha + ")";
            context.beginPath();
            context.arc(projX, projY, Math.abs(m * particleRad), 0, pi2, false);
            context.closePath();
            context.fill();
        //}
    }
}
//=============
// particle generator.
// params are from the particle emitter
//
function ballGenerator(x, y, w, h) {

    var theta = rnd(0, pi2),
        phi   = Math.acos(rnd(-1,+1)),
        x0    = sphereRad * Math.sin(phi) * Math.cos(theta),
        y0    = sphereRad * Math.sin(phi) * Math.sin(theta),
        z0    = sphereRad * Math.cos(phi);

    return Particle.Factory({

        life:   particleLifeSpan * rnd(0.4, 1.4),
        resize: rnd(-1, 1),
        fadeIn: true,
        fadeOut: true,

        // bound to particle
        draw: drawBall,

        data: {
            x: x0 + sphereCenterX,
            y: y0 + sphereCenterY,
            z: z0 + sphereCenterZ,
            velX: 0.002 * x0,
            velY: 0.002 * y0,
            velZ: 0.002 * z0,
            age:  0
        }
    });
}

// each frame of animation
//
function onFrame() {
    status.turnAngle = (status.turnAngle + turnSpeed) % pi2;
}


var Spheres = function(canvas) {

    var r = canvas.getBoundingClientRect();

    canvasWid = r.width;
    canvasHt  = r.height;
    sphereCenterX = projCenterX = 0;
    sphereCenterY = canvasHt;

    // this emitter-constructor doesn't create or initialize
    // any particles; it just sets up their emitter
    //
    return Emitter.Factory(canvas, {
        max:       maxParticles,
        generator: ballGenerator,
        onFrame:   onFrame,
        addPerSec: addPerSec
    });
};

module.exports = Spheres;

