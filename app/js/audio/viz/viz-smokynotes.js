"use strict";

var VizUtils       = require('./utils'),
    ParticleImages = require('./particleImages'),
    Particle       = require('../../particle/particle'),
    Emitter        = require('../../particle/emitter'),
    UA             = require('../../tools/ua');

var particleLifeSpan = 1800,    // lose 20% every second
    maxParticles     = 40,
    maxVelocity      = 2.5,
    losePerSecPct    = 1000/particleLifeSpan,   // 1000 is const
    addPerSec        = maxParticles * losePerSecPct,
    smokeImage       = new Image(),
    smokeWid,
    smokeHt,
    rnd              = VizUtils.rnd,
    blowSmoke        = !(UA.firefox || UA.safari);


// load the smoke image
//
smokeImage.addEventListener('load', function () {
    smokeWid = this.width;
    smokeHt = this.height;
});
smokeImage.src = 'i/smoke-superlight.png';


//=============
// drawing function. bound to particle
//
function drawSmokyParticle(context, x, y, width, height, lifePercent) {

    var ct,
        sWidth,
        sHeight;

    if (blowSmoke) {
        // initialize the smoke particles
        if (!this.smokes) {
            ct = 2;     // # smokes per particle
            this.smokes = [];
            while (ct--)
                this.smokes.push({
                    x:      rnd(-4, +4),
                    y:      rnd(-4, +4),
                    xVel:   0,
                    yVel:   0,
                    rot:    Math.PI * rnd(-2, +2),
                    rotVel: Math.PI * rnd(-0.0015, 0.0015)
                })
        }

        // scale our puffs
        sWidth  = smokeWid * (1 + 5 * lifePercent);
        sHeight = smokeHt  * (1 + 5 * lifePercent);

        // draw the smoke particles
        this.smokes.forEach(function (smoke) {

            context.save();

            context.rotate(smoke.rot);
            context.translate(
                x + smoke.x - sWidth / 2,
                y + smoke.y - sHeight / 2
            );
            context.drawImage(smokeImage, 0, 0, sWidth, sHeight);

            context.restore();

            smoke.rot += smoke.rotVel;
            smoke.x += smoke.xVel;
            smoke.y += smoke.yVel;
        });
    }

    // draw the note
    ParticleImages.render(this.data.noteID, context, x, y, width, height);
}

//=============
// particle generator.
// params are from the particle emitter
//
function smokyNoteGenerator(x, y, w, h) {

    var size = rnd(20,50, true);

    return Particle.Factory({

        // start coords (in the lower left)
        x:      rnd(40, 60, true),
        y:      rnd(h - 60, h - 40, true),
        w:      size,
        h:      size,
        xVel:   rnd(0, +maxVelocity),  // velocity (x & y)
        yVel:   rnd(0, -maxVelocity),

        rot:    Math.PI * rnd(-2, +2),
        rotVel: Math.PI * rnd(-0.0015,0.0015),
        fadeIn: true,
        fadeOut: true,

        // randomize the lifespan a bit, so that
        // they regenerate continuously & smoothly
        life:   particleLifeSpan * rnd(0.5, 1.8),
        resize: rnd(-1, 1),

        // bound to particle
        draw:   drawSmokyParticle,

        // store this data for retrieval during the draw callback
        data: {
            noteID: ParticleImages.pickRandom(rnd(0,1))
        }
    });
}

var SmokyNotes = function(canvas) {

    // this emitter-constructor doesn't create or initialize
    // any particles; it just sets up their emitter
    //
    return Emitter.Factory(canvas, {
        x:         Math.ceil(window.innerWidth / 2),
        y:         Math.ceil(window.innerHeight / 2),
        max:       maxParticles,
        generator: smokyNoteGenerator,
        addPerSec: addPerSec
    });
};

module.exports = SmokyNotes;
