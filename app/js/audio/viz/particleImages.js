"use strict";

/*
    this module loads up images for use by
    our particle generator

    to add new images to the palette,
    just add a new entry to the particleImages object.
    the image files go in i/notes.

    note: this would be better with SVGs, but
    not all browsers do well with fading them out :(
    (lookin' at you, firefox.)

*/

var particleImages = {
        qNote: {
            src: 'quarter.png',
            odds: 20
        },
        dotEighth: {
            src: 'dot-eighth.png',
            odds: 20
        },
        32: {
            src: '32.png',
            odds: 1
        },
        sharp: {
            src: 'sharp.png',
            odds: 10
        },
        flat: {
            src: 'flat.png',
            odds: 10
        },
        68: {
            src: '6-8.png',
            odds: 1
        },
        bassClef: {
            src: 'bass-clef.png',
            odds: 1
        }
    },
    oddsCt = 0;

function prepOneImage(imageInfo) {

    var i = new Image();
    i.addEventListener('load', function () {
        imageInfo.img = i;
        imageInfo.w   = this.width;
        imageInfo.h   = this.height;
    });
    i.src = 'i/notes/' + imageInfo.src;

    // tally for calculating odds of random selection
    oddsCt += imageInfo.odds;
}

for (var imageCode in particleImages) {
    if (particleImages.hasOwnProperty(imageCode))
        prepOneImage(particleImages[imageCode]);
}

var ParticleImages = {

    // pass in a random number [0-1], we'll give you random ID which you can pass to render().
    // use the "odds" property to weight svgs differently
    //
    pickRandom: function(r) {
        var imageCode, imageInfo, runningPct = 0;
        for (imageCode in particleImages) {
            if (particleImages.hasOwnProperty(imageCode)) {
                imageInfo = particleImages[imageCode];
                runningPct += imageInfo.odds / oddsCt;
                if (r < runningPct)
                    return imageCode;
            }
        }
    },
    render: function(id, context, x, y, w, h) {
        var imageInfo = particleImages[id];
        if (imageInfo && imageInfo.img)     // guard against late file loads
            context.drawImage(imageInfo.img, x, y, imageInfo.w, imageInfo.h);
    }
};

module.exports = ParticleImages;