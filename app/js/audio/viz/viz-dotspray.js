"use strict";

var VizUtils       = require('./utils'),
    Particle       = require('../../particle/particle'),
    Emitter        = require('../../particle/emitter');

var particleLifeSpan = 2500,    // lose 20% every second
    maxParticles     = 80,
    maxVelocity      = 3.8,
    losePerSecPct    = 1000/particleLifeSpan,   // 1000 is const
    addPerSec        = maxParticles * losePerSecPct,
    rnd              = VizUtils.rnd;

//=============
// particle generator.
// params are from the particle emitter
//
function dotGenerator(x, y, w, h) {

    var size = rnd(20,50, true);

    return Particle.Factory({

        // start coords (in the lower left)
        x:      rnd(90, 140, true),
        y:      rnd(h - 20, h - 30, true),
        w:      size,
        h:      size,
        xVel:   maxVelocity * rnd(0.1, 1),
        yVel:   maxVelocity * rnd(-1, -0.1), // 0, -maxVelocity),

        r:      rnd(64, 128, true),
        b:      rnd(128, 255,  true),
        g:      rnd(64, 96,  true),

        fadeIn: true,
        fadeOut: true,

        // uniform life span for all particles. this makes
        // them spray out in batches
        life:   particleLifeSpan,
        resize: rnd(-1, 1),

        // bound to particle
        draw: Particle.proto.drawCircleGradient
    });
}

var DotSpray = function(canvas) {

    // this emitter-constructor doesn't create or initialize
    // any particles; it just sets up their emitter
    //
    return Emitter.Factory(canvas, {
        x:         Math.ceil(window.innerWidth / 2),
        y:         Math.ceil(window.innerHeight / 2),
        max:       maxParticles,
        generator: dotGenerator,
        addPerSec: addPerSec
    });
};

module.exports = DotSpray;
