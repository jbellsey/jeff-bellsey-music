"use strict";

var VizUtils       = require('./utils'),
    Particle       = require('../../particle/particle'),
    Emitter        = require('../../particle/emitter');

var particleLifeSpan = 1200,    // lose 20% every second
    maxParticles     = 20,
    maxVelocity      = 5,
    losePerSecPct    = 1000/particleLifeSpan,   // 1000 is const
    addPerSec        = maxParticles * losePerSecPct,
    rnd              = VizUtils.rnd;


// bound to particle
//
function drawBall(context, x, y, width, height, lifePercent) {

    var ballSizeFactor = 3, // * lifePercent,
        ballSize = this.data.ballSize * ballSizeFactor, // Math.abs(ballSizeFactor*(vx+vy)),

        // a little trig never hurt anyone. get the triangle's
        // endpoints using the angle we pre-calculated earlier
        //
        vx = ballSizeFactor * this.data.cos,
        vy = vx * this.data.velRatio,

        // find the two corners of the triangle
        tri1 = {
            x: x - vy * 2,
            y: y + vx * 2
        },
        tri2 = {
            x: x + vy * 2,
            y: y - vx * 2
        };

    // make a triangle
    context.beginPath();

    var grad1 = context.createLinearGradient(x,y, this.data.startX, this.data.startY);
    grad1.addColorStop(0, 'rgba(222,222,255,0.3)');
    grad1.addColorStop(1, '#2b2e4c');
    context.fillStyle = grad1;

    context.moveTo(this.data.startX, this.data.startY);
    context.lineTo(tri1.x, tri1.y);
    context.lineTo(tri2.x, tri2.y);
    context.closePath();
    context.fill();

    //and a ball-end
    context.beginPath();
    var gradient = context.createRadialGradient(
        x, y, 0,
        x, y, ballSize);
    gradient.addColorStop(0,   'rgba(200,200,222,1)');
    gradient.addColorStop(0.4, 'rgba(200,200,222,1)');
    gradient.addColorStop(1,   'rgba(200,200,222,0)');
    context.fillStyle = gradient;

    context.arc(x,  y, ballSize, 0, 2 * Math.PI);
    context.fill();
}
//=============
// particle generator.
// params are from the particle emitter
//
function ballGenerator(x, y, w, h) {

    var size = rnd(20,50, true),
        startX = 0,
        startY = h,
        vx = rnd(0.01, +maxVelocity),
        vy = rnd(0.01, -maxVelocity),
        angle = Math.atan(vy/vx);

    return Particle.Factory({

        // start coords (in the lower left)
        x:      startX,
        y:      startY,
        w:      size,
        h:      size,
        xVel:   vx,
        yVel:   vy,

        life:   particleLifeSpan * rnd(0.4, 1.4),
        resize: rnd(-1, 1),
        fadeIn: true,
        fadeOut: true,
        fadeInflex: 0.7,

        // bound to particle
        draw: drawBall,
        
        data: {
            startX: startX,
            startY: startY,
            ballSize: rnd(4,7),

            cos: Math.cos(angle),
            velRatio: vy / vx
        }
    });
}


var FlyBalls = function(canvas) {

    // this emitter-constructor doesn't create or initialize
    // any particles; it just sets up their emitter
    //
    return Emitter.Factory(canvas, {
        max:       maxParticles,
        generator: ballGenerator,
        addPerSec: addPerSec
    });
};

module.exports = FlyBalls;

