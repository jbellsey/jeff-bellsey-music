"use strict";

// my kingdom for a loop.
//
var allVisualizations = {
    dotspray:   require('./viz-dotspray'),
    flyballs:   require('./viz-flyballs'),
    pulsar:     require('./viz-pulsar'),
    smokynotes: require('./viz-smokynotes'),
    sphere:     require('./viz-sphere')
};

/**
 * this file sets up the actual visualizations during
 * audio playback. it is used by the AudioButton object,
 * and this object has intimate knowledge of that one.
 */

var Visualizations = function(viz, audioBtn, vizStyle) {

    var emitter;

    // the vizStyle passed in should match a filename in this folder
    //
    try {
        emitter = allVisualizations[vizStyle](document.getElementById('bigCanvas'));
    }
    catch(e) {
        emitter = null;
    }

    // public API
    //
    return {
        // DOM structure has been changed... force reevaluations.
        // noop for now. made available for the future, when we
        // add in FFT-based visualizations.
        //
        reset: function() {},

        audioStart: function() {
            if (emitter) {
                emitter.makeParticles();
                emitter.restart(true);
            }
        },

        audioStop: function() {
            if (emitter)
                emitter.stop();
        }
    }
};

module.exports = {

    Factory: function(viz, audioBtn, vizStyle) {
        return new Visualizations(viz, audioBtn, vizStyle);
    }
};