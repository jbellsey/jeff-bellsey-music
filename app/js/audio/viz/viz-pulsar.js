"use strict";

var VizUtils       = require('./utils'),
    Particle       = require('../../particle/particle'),
    Emitter        = require('../../particle/emitter');

var particleLifeSpan = 1600,
    maxParticles     = 20,
    losePerSecPct    = 1000/particleLifeSpan,   // 1000 is const
    addPerSec        = maxParticles * losePerSecPct,
    rnd              = VizUtils.rnd;


//=============
// drawing function. bound to particle
//
function drawPulsar(context, x, y, width, height, lifePercent) {

    // the center circle
    context.fillStyle = 'rgba(' + this.r + ',' + this.g + ',' + this.b + ',1)';
    context.beginPath();
    context.arc(x, y, width/2, 0, 2 * Math.PI, false);
    context.fill();
}

//=============
// particle generator.
// params are from the particle emitter
//
function pulsarGenerator(x, y, w, h) {

    var size = rnd(20,50, true),
        col = 100;

    return Particle.Factory({

        // start coords (in the lower left)
        x:      rnd(50, 500, true),
        y:      rnd(h - 50, 500, true),
        w:      size,
        h:      size,

        r:      col,
        b:      col,
        g:      col,

        fadeIn: false,
        fadeOut: false,

        life:   particleLifeSpan * rnd(0.5, 1.3),
        resize: 0,

        // bound to particle
        draw: drawPulsar
    });
}

var Pulsar = function(canvas) {

    // this emitter-constructor doesn't create or initialize
    // any particles; it just sets up their emitter
    //
    return Emitter.Factory(canvas, {
        x:         Math.ceil(window.innerWidth / 2),
        y:         Math.ceil(window.innerHeight / 2),
        max:       maxParticles,
        generator: pulsarGenerator,
        addPerSec: addPerSec
    });
};

module.exports = Pulsar;
