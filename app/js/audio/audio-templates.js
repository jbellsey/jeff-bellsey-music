
// some HTML & SVG template code for use
// by the audioButton module
//
module.exports = {

    audio: [
               '<audio preload="metadata">',
               '<source src="mp3/@@@.mp3" type="audio/mpeg" />',
               '</audio>'
           ].join(''),

    // the SVG code for the play button. it needs to be
    // inserted inline to be targetable by CSS
    //
    playSVG: [
               '<svg version="1.1" x="0px" y="0px" width="17.625px"',
               'height="17.625px" viewBox="0 0 17.625 17.625"',
               'enable-background="new 0 0 17.625 17.625" xml:space="preserve">',
               '<radialGradient id="SVGID_1_" cx="8.8125" cy="8.8125" r="8.8125" gradientUnits="userSpaceOnUse">',
               '<stop  offset="0" style="stop-color:#3FABC9"/>',
               '<stop  offset="1" style="stop-color:#155D82"/>',
               '</radialGradient>',
               '<circle fill="url(#SVGID_1_)" cx="8.812" cy="8.812" r="8.812"/>',
               '<g id="icon"></g>',
               '</svg>'
           ].join(''),

    // play pause SVG icons, in recipe form. see usage comments in setPlayIcon()
    icons: {
        play: [
            {
                elt: 'polygon',
                attrs: {
                    points: '6.76,4.703 12.146,8.771 6.76,12.838',
                    fill: '#000'
                }
            }
        ],
        pause: [
            {
                elt: 'rect',
                attrs: {
                    x: 5.5,
                    y: 5.5,
                    width: 2.5,
                    height: 7,
                    fill: '#000'
                }
            },
            {
                elt: 'rect',
                attrs: {
                    x: 9.5,
                    y: 5.5,
                    width: 2.5,
                    height: 7,
                    fill: '#000'
                }
            }
        ]
    },

    // DOM structure for the play bar
    playBarTemplate: [
               '<div class="playBarWrap">',
               '<div class="playStatus">',
               '<div class="time"></div>',
               '<div class="barbox">',
               '<div class="buffer"></div>',
               '<div class="bar"></div>',
               '</div>',
               '</div>',
               '</div>'
           ].join('')
};
