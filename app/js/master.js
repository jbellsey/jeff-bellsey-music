"use strict";

// we need to load these "orphan" modules to kickstart them.
//
require('./groupButtons/group-button-manager');
require('./groupButtons/utils-menu');
require('./animations/wheel');

var DomReady = require('./tools/dom-ready'),
    GA       = require('./tools/ga');

// log message for developers. coded to persist through
// the build process, which strips console.log messages
//
var C = console;
[
    '-------------------------------------------',
    '--- Greetings techie! This site has a repo:',
    '---',
    '---   https://bitbucket.org/jbellsey/jeff-bellsey-music',
    '---',
    '--- and my web site is:',
    '---',
    '---   http://futureground.net',
    '-------------------------------------------'
].forEach(function(s){
        C.log.call(C, s)
    });

// all outbound links get special treatment
//
function trackOutboundLink(e) {
    GA.event('Outbound Link', e.target.href);
}

DomReady(function() {
    [].forEach.call(document.querySelectorAll('a.outbound'), function(a) {
        a.setAttribute('target', '_blank');
        a.addEventListener('click', trackOutboundLink);
    });
});
