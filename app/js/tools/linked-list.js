"use strict";

/*
    a simple, special-purpose linked list. could easily be
    adapted to use an object pool, if that becomes needed.
    used by our particle generator instead of arrays
*/

function LinkedList() {
    this.first = null;
    this.last  = null;
    this.size  = 0;
}

LinkedList.prototype.push = function(data) {

    // the basic node data structure:
    var n = {
        data: data,
        prev: null,
        next: null
    };

    // first item to be added to the LL?
    if (this.first === null) {
        this.first = n;
        this.last  = n;
    }
    else {
        this.last.next = n;
        n.prev = this.last;
        this.last = n;
    }

    ++this.size;
};

// our "forEach". the callback takes one param (the data).
// it should return TRUE to keep the node, FALSE to delete it
//
LinkedList.prototype.loop = function(cb) {
    var n = this.first,
        nxt, prv;
    while (n) {
        nxt = n.next;
        prv = n.prev;

        // callback returns false to delete this node.
        // we embed the deletion operation here for
        // convenience (and speed).
        //
        if (!cb(n.data)) {

            if (prv !== null)
                prv.next = nxt;
            else
                this.first = nxt;

            if (nxt !== null)
                nxt.prev = prv;
            else
                this.last = prv;

            --this.size;
        }
        n = nxt;
    }
};

LinkedList.prototype.length = function() {
    return this.size;
};

module.exports = LinkedList;
