/*
    There are times that Modernizr is insufficient.
    In this case, animating PNGs inside a canvas is
    crazy slow in FF and Safari.
*/

var ua = navigator.userAgent.toLowerCase(),
    uadata = {};

// for great code size
function has(x) {return ua.indexOf(x) > -1;}

if (has("chrome"))
    uadata.chrome = true;
else if (has("firefox"))
    uadata.firefox = true;
else if (has("opera"))
    uadata.opera = true;
else if (has("msie"))
    uadata.ie = true;
else if (has("safari"))
    uadata.safari = true;

uadata.ios = has('uiwebviewforstaticfilecontent');
uadata.mobile =  has('mobi') || uadata.ios;


module.exports = uadata;
