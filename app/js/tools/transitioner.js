'use strict';

/* jshint unused:true */

/*
    this class lets you do a one-time transition on an object.
    it doesn't install RAF; it just adds a class to an element,
    then removes it when the animation completes.

    it also adds the 'transitioner' class to the object. you
    can use this to ensure that the animation duration is set properly.

    e.g.:
        var elem = document.getElementById('trigger');
        document.getElementById('animator').addEventListener('click', function () {
            if (showing)
                elem.animate('bounceOutLeft', 'endStatus');
            else
                elem.animate('bounceInRight');
            showing = !showing;
        });
*/


// figure out which animation end event (aee) to use.
// private & local
//
var aee = (function(){  // from modernizr
    var a, el = document.createElement('fakeelement'),
        all = {
            'animation':      'animationend',
            'OAnimation':     'oAnimationEnd',
            'MozAnimation':   'animationend',
            'WebkitAnimation':'webkitAnimationEnd'
        };
    for (a in all)
        if (el.style[a] !== undefined)
            return all[a];
})();

function Transitioner(obj) {


    // public API
    return {

        /* OPTIONS is an object:
             {
                 animClass: '',      // required
                 removeAtStart: '',
                 removeAtEnd: '',
                 addAtEnd: '',
                 waitForThese: null,    // nodelist
                 backstopTime: 0,
                 cb: ''
             }

             the "waitForThese" nodelist can be provided if there are
             other simultaneous animations running. (e.g., children
             sliding in.)  a listener will be installed on each node
             in the list, so be sure they're all animating!!

             the "backstopTime" option is provided to ensure we don't
             wait forever for "waitForThese" animations. a timeout
             will be installed if you pass in a MS count here.
        */
        animate: function (options) {

            var waitForCt  = 1,
                receivedCt = 0,
                guard      = false;

            if (options.removeAtStart)
                obj.classList.remove(options.removeAtStart);

            obj.classList.add(options.animClass);

            // after the animation is complete
            //
            function done() {
                if (guard) return;  // once only
                guard = true;

                if (options.addAtEnd)
                    obj.classList.add(options.addAtEnd);
                if (options.removeAtEnd)
                    obj.classList.remove(options.removeAtEnd);

                obj.classList.remove(options.animClass);

                if (options.cb)
                    options.cb(obj);
            }

            // end-event listener. must be BOUND. use "obj" carefully;
            // prefer "this" for reference to the element
            //
            function eeL(ev) {
                ev.stopPropagation();

                // this is a one-time event; cancel our subscription
                this.removeEventListener(aee, eeL);

                if (++receivedCt >= waitForCt)
                    done();
            }
            // subscribe to the animation-end event
            obj.addEventListener(aee, eeL.bind(obj));

            // and on the requested nodelist, if provided
            if (options.waitForThese) {

                waitForCt += options.waitForThese.length;
                [].forEach.call(options.waitForThese, function (elt) {
                    elt.addEventListener(aee, eeL.bind(elt));
                });

                // install a backstop timeout, if requested
                if (options.backstopTime)
                    setTimeout(done, +options.backstopTime);
            }

            obj.classList.add(options.animClass);
        }
    };
}

module.exports = {

    Factory: function(obj) {
        return new Transitioner(obj);
    }
};