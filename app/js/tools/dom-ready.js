/*
 manager for domReady event. if you
 have code that needs to wait, install
 it as a callback into this module:

 var domReady = require('./domReady');
 domReady(function() {
 // initialization code
 });
 */

var callbacks = [],
    doc       = document,
    eventName = 'DOMContentLoaded',

    // by the time this code runs, the document might be ready
    // for us. in which case, we can run callbacks immediately
    // rather than deferring them.
    //
    // http://www.w3schools.com/jsref/prop_doc_readystate.asp
    //
    domReady  = /^loaded|^inter|^comp/.test(doc.readyState),

    listener = function() {
        doc.removeEventListener(eventName, listener);
        domReady = true;
        callbacks.forEach(function(cb) {
            cb();
        });
    };

doc.addEventListener(eventName, listener);

module.exports = function(cb) {
    if (domReady)
        cb();
    else
        callbacks.push(cb);
};