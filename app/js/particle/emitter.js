"use strict";

var ParticleEngine = require('./engine'),
    LinkedList = require('../tools/linked-list');

// this is a master switch for all emitters.
// we let the user disable visualizations
//
var allowNewParticles = true;

/**
 * A class which allows us to create Emitters of particles.
 * @param {Number}   opts.x          emitter location, for fireworks or other sprays
 * @param {Number}   opts.y
 * @param {Number}   opts.max        max # particles
 * @param {Boolean}  [opts.emit]     whether to start emitting now. default = false
 * @param {Boolean}  opts.startCt    # particles to initialize now
 * @param {Function} opts.generator  particle generator function
 * @param {Function} [opts.onFrame]  function called once per frame, regardless # particles
 * @param {Number}   opts.addPerSec  # of particles to add, to account for dying particles.
 *                                   only relevant if this emitter is currently emitting
 */
function Emitter(canvas, opts) {
    var r = canvas.getBoundingClientRect();
    this.x                = opts.x || 0;
    this.y                = opts.y || 0;
    this.width            = r.width;
    this.height           = r.height;
    this.maxParticles     = opts.max || 500;
    this.onFrameCB        = opts.onFrame || null;
    this.generateParticle = opts.generator;
    this.emit             = !!(opts.emit || false);
    this.addPerSec        = opts.addPerSec || 0;    // if not specified, this emitter doesn't add any particles by itself

    // prep initial particles. they're stored as a linked list, rather
    // than an array, because we're planning on adding and deleting
    // them a LOT, one at a time.
    this.particles = new LinkedList;
    this.makeParticles(opts.startCt || 0);

    // tell the master controller
    ParticleEngine.addEmitter(canvas, this);
}

// the engine gives us a special API for accessing key methods
Emitter.prototype.setEngine = function(e) {
    this.engine = e;
};

Emitter.prototype.makeParticles = function(n) {

    if (!allowNewParticles)
        return;

    if (typeof n !== 'number' || n < 0)
        n = this.maxParticles/4;

    // speed up the loop by caching property lookups. yes, it's fine-tuny
    var x = this.x,
        y = this.y,
        w = this.width,
        h = this.height,
        p = this.particles;

    while (n--)
        p.push(this.generateParticle(x,y,w,h));
};

Emitter.prototype.restart = function(andBeginEmitting) {
    if (!!andBeginEmitting)
        this.emit = true;
    if (this.engine)
        this.engine.restart(this);
};

Emitter.prototype.stop = function() {
    this.emit = false;
};

/**
 * Called inside rAF loop
 * @param   context   (canvas context)
 * @param   elapsedMS (time since the last frame, or 0)
 * @returns {boolean} (true if we want more frames)
 */
Emitter.prototype.frame = function(context, elapsedMS) {

    // do we need to add more particles?
    if (this.emit)
        this.topOffParticles(elapsedMS);

    // allow emitters to have their own per-frame actions
    if (this.onFrameCB)
        this.onFrameCB.call(this, context, elapsedMS);

    this.particles.loop(function(p) {

        // drawing is done here
        p.render(context);

        // update particle properties for the next cycle
        // if the particle has died, remove it
        return !p.growOlder(elapsedMS);
    });

    return this.particles.length() > 0;
};

// add some more particles to the emitter, if needed
//
Emitter.prototype.topOffParticles = function(elapsedMS) {

    var diff        = this.maxParticles - this.particles.length(),
        addPerFrame = Math.max(1, Math.ceil(this.addPerSec * (elapsedMS/1000))),
        addNow      = Math.min(diff, addPerFrame);

    this.makeParticles(addNow);
};

module.exports = {

    Factory: function(canvas, opts) {
        return new Emitter(canvas,opts);
    },

    // turn all visualizations on or off here
    MasterSwitch: function(onOrOff) {
        var newStatus = !!onOrOff;
        if (allowNewParticles !== newStatus)
            this.ToggleMasterSwitch();
    },
    ToggleMasterSwitch: function() {
        allowNewParticles = !allowNewParticles;
        if (allowNewParticles)
            ParticleEngine.restartAll();
        return allowNewParticles;
    }
};
