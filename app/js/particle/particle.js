"use strict";

function Particle() {}
var PP = Particle.prototype;

/**
 * @param {Number} opts.x       initial position
 * @param {Number} opts.y
 * @param {Number} opts.w       dimensions
 * @param {Number} opts.h
 * @param {Number} [opts.rot]   initial rotation  (in radians)
 * @param {Number} [opts.rotVel] rotation velocity (in radians)
 * @param {Number} opts.xVel    velocity
 * @param {Number} opts.yVel
 * @param {Number} opts.life    lifespan of this particle (in MS)
 * @param {Number} opts.resize  growth over time: [0: no change, <0: shrink, >0: grow]
 * @param {Number} opts.r       particle color
 * @param {Number} opts.g
 * @param {Number} opts.b
 * @param {Boolean}[opts.fadeOut] defaults to true. set to false to prevent global alpha fade
 * @param {Boolean}[opts.fadeIn] defaults to false
 * @param {Number} [opts.fadeInflex] if you're fading IN and OUT (both), set the inflection point,
 *                              at we switch from fading in to fading out. default = 0.2
 * @param {Number} [opts.decay] callback. set to override default motion & rotation. bound to particle.
 * @param {Number} [opts.draw]  callback. set to override draw operation. bound to particle.
 * @param {(String|Object)} [opts.image]  can be "url string" or {new Image()} object. uses its own custom
 *                              draw callback, so beware of providing your own, as it shouldn't be needed.
 * @param {Number} [opts.data]  will be kept for future reference
 */

PP.init = function(opts) {
    this.x          = opts.x      || 0;
    this.y          = opts.y      || 0;
    this.width      = opts.w      || 150;
    this.height     = opts.h      || 150;
    this.rotation   = opts.rot    || 0;
    this.rVelocity  = opts.rotVel || 0;
    this.xVelocity  = opts.xVel   || 0;
    this.yVelocity  = opts.yVel   || 0;
    this.fadeOut    = (typeof opts.fadeOut === 'undefined') ? true : !!opts.fadeOut;
    this.fadeIn     = !!opts.fadeIn;
    this.fadeInflex = (this.fadeIn && this.fadeOut ? (opts.fadeInflex || 0.2) : 0);
    this.maxLife    = opts.life   || 100;
    this.lifeLeft   = this.maxLife;
    this.resize     = opts.resize;
    this.r          = opts.r;
    this.g          = opts.g;
    this.b          = opts.b;
    this.data       = opts.data;

    // user-overridable callbacks
    //
    this.opacityCB  = opts.opacity || this.setOpacity;
    this.decayCB    = opts.decay   || this.decay;
    this.drawCB     = opts.draw    || this.drawRect;

    if (opts.image) {
        // image particles use a different default draw callback.
        // user can still override it
        //
        if (!opts.draw)
            this.drawCB = this.drawImage.bind(this);

        if (typeof opts.image === 'object') {
            this.img = opts.image;
            this.loadImage(opts.image);
        }
        else {
            // load from URL
            this.imageLoaded = false;
            this.img = new Image();

            var self = this;
            this.img.addEventListener('load', function () {
                self.loadImage(this);
            });
            this.img.src = opts.image;
        }
    }
};

/**
 * @param elapsedMS
 * @returns {boolean} true if this particle has died
 */
PP.growOlder = function(elapsedMS) {

    // users can override the decay function (in the
    // constructor), for customized evolution-over-time
    //
    this.decayCB.call(this, elapsedMS);

    this.lifeLeft -= elapsedMS;
    return this.lifeLeft < 0;
};

/**
 * default decay function. can be overridden in constructor, using "decay" callback.
 * will be bound to Particle object either way.
 *
 * @param elapsedMS   time since last frame
 */
PP.decay = function(elapsedMS) {
    this.x += this.xVelocity;
    this.y += this.yVelocity;
    if (this.rotation)
        this.rotation += this.rVelocity;
};

/**
 * all the drawing setup; the emitter calls this method.
 * the actual drawing is done in Particle.draw, or in a
 * user-provided callback.
 */
PP.render = function(context) {

    var width  = this.width,
        height = this.height,
        lifePercent = 1 - (this.lifeLeft / this.maxLife);   // 0 = born, 1 = done

    // decrease size with life of particle.
    if (this.resize < 0) {
        width  *= (1 - lifePercent * 0.6);  // don't shrink down to zero
        height *= (1 - lifePercent * 0.6);
    }
    // increase size with life of particle.
    else if (this.resize > 0) {
        width  *= (1 + 2 * lifePercent);    // this factor makes for a nicer expansion curve
        height *= (1 + 2 * lifePercent);
    }

    // user can disable fadeout, and optionally apply a custom
    // opacity curve in the draw callback
    this.opacityCB.call(this, context, lifePercent);

    // if rotation is requested, we have a bit more setup
    if (this.rotation !== 0) {

        context.save();

        context.translate(this.x, this.y);
        context.rotate(this.rotation);

        // the actual drawing is done here
        this.drawCB.call(this, context, 0, 0, width, height, lifePercent);

        context.restore();
    }
    else {
        this.drawCB.call(this, context, this.x, this.y, width, height, lifePercent);
    }
};

PP.setOpacity = function(context, lifePercent) {

    if (this.fadeIn) {
        var inflection = this.fadeInflex;
        if (lifePercent < inflection)
            context.globalAlpha = lifePercent / inflection;
        else if (this.fadeOut)
            context.globalAlpha = 1 - (lifePercent - inflection)/(1 - inflection);
    }
    else if (this.fadeOut)
        context.globalAlpha = 1 - lifePercent;

};

//=================
// "BASE CLASS": [Rectangle]
//
PP.drawRect = function(context, x, y, width, height, lifePercent) {

    context.fillStyle = 'rgba(' + this.r + ',' + this.g + ',' + this.b + ',1)';

    //context.strokeStyle = '#fff';
    //context.strokeRect(x - width/2, y - height/2, width, height);

    context.fillRect(x - width/2, y - height/2, width, height);
};

//=================
// "SUBCLASS": [Circle]
// (note: this is not actually a subclass, but the installation
//  of this callback is essentially convenient polymorphism)
//
// usage: when creating a particle, pass this in {opts}:
//    draw: PP.drawCircle
//
PP.drawCircle = function(context, x, y, width, height, lifePercent) {

    context.fillStyle = 'rgba(' + this.r + ',' + this.g + ',' + this.b + ',1)';

    context.beginPath();
    context.arc(x, y, width/2, 0, 2 * Math.PI, false);
    context.fill();
};

//=================
// "SUBCLASS": [CircleGradient]
//
PP.drawCircleGradient = function(context, x, y, width, height, lifePercent) {

    // create radial gradient
    var color = 'rgba(' + this.r + ',' + this.g + ',' + this.b + ','; // opacity added in a moment...
    var gradient = context.createRadialGradient(x, y, 0, x, y, width/2);
    gradient.addColorStop(0, color + (1 - lifePercent) + ')');
    gradient.addColorStop(1, color + '0)');
    context.fillStyle = gradient;

    // and fill the circle
    context.beginPath();
    context.arc(x, y, width/2, 0, 2 * Math.PI, false);
    context.fill();
};

//=================
// "SUBCLASS": [Image]
// usage: pass in {opts.image} in the constructor. if you do that,
//  the callback below will be installed for you.
//
PP.loadImage = function(i) {
    this.imageLoaded = true;
};

PP.drawImage = function(context, x, y, width, height, lifePercent) {

    if (this.imageLoaded)
        context.drawImage(this.img, x - width/2, y - height/2, width, height);
};

module.exports = {

    // use a factory. this will let us add object pooling later
    //
    Factory: function(opts) {
        var p = new Particle;
        p.init(opts);
        return p;
    },

    // easy access to the prototype. this allows
    // callers to reference our convenience methods
    // when assigning callbacks (for draw, eg)
    //
    proto: Particle.prototype
};