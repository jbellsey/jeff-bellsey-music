"use strict";
/*
    this module tracks all emitters for our particle system,
    and runs the actual animation loop
*/

var emitters = [],
    lastTime = -1,
    anyOn    = false,
    emitterAPI;

function resetTimer() {
    lastTime = -1;
}

// main animation loop to update each particle's state and redraw the scene.
//
function animate(curTime) {

    var emitter,
        thisCanvas,
        clearedCanvases = [],
        n = emitters.length,    // iterate through all emitters
        elapsedMS = (lastTime > 0) ? curTime - lastTime : 0;

    lastTime = curTime || -1;

    // since we're running multiple emitters, we keep track of which
    // one(s) of them are still "on" (meaning, still need animation
    // frames). those that aren't on get skipped during the loop below.
    // and when all emitters are off, we stop asking for frames altogether.
    // this is why we have a "restart" API that emitters can call when
    // they get turned on again.
    //
    anyOn = false;

    while (n--) {
        emitter = emitters[n];
        if (emitter.on) {

            // clear the canvas, but guard against repeat clearings on shared canvases
            //
            thisCanvas = emitter.canvas;
            if (clearedCanvases.length === 0 || clearedCanvases.indexOf(thisCanvas) < 0) {
                emitter.context.clearRect(0, 0, thisCanvas.width, thisCanvas.height);
                if (n > 0)
                    clearedCanvases.push(thisCanvas);
            }

            // run one frame, and track whether the
            // emitter wants more frames
            //
            emitter.on = emitter.e.frame(emitter.context, elapsedMS);

            // if this was the last frame, clear the context now
            if (emitter.on)
                anyOn = true;
            else
                emitter.context.clearRect(0, 0, thisCanvas.width, thisCanvas.height);
        }
    }

    // only do another frame if any emitters are still on
    if (anyOn)
        requestAnimationFrame(animate);
}

// a separate API is passed to emitters as a
// way to reach back into this module
//
emitterAPI = {
    restart: function(e) {

        // find the indicated emitter in our private array,
        // so we can flag it as now running
        //
        emitters.forEach(function(emit) {
            if (!e || emit.e === e) {
                emit.on = true;
            }
        });

        // if this emitter is the only one running,
        // reinstall the rAF handler
        //
        if (!anyOn) {
            resetTimer();
            animate();
        }
    }
};

// public API
//
module.exports = {

    addEmitter: function(canvas, e) {

        var r = canvas.getBoundingClientRect();
        canvas.width = r.width;
        canvas.height = r.height;

        // wrap the emitter in an object with some
        // other useful metadata
        //
        emitters.push({
            e: e,
            canvas: canvas,
            context: canvas.getContext('2d'),
            on: true
        });

        e.setEngine(emitterAPI);
        return e;
    },

    restartAll: function() {
        emitterAPI.restart();
    }
};