# Particle Engine

The particle engine is broken into three modules.

## Engine

The Engine module is the master controller, and runs the animation
loop. Consumers don't need to interact with the engine directly.
Instead, just create an Emitter, and attach it to a customized Particle
generator. The Emitter will communicate with the Engine as needed.

## Emitter

The Emitter manages Particles, generating them and deleting them
as needed. It gets pinged by the Engine at each animation frame,
then tells the Particles to render themselves. 

Emitters are fairly generic. They have a few options for managing
the particles' lifespan-related attributes
(e.g., do they fade in? fade out? both?), but most
of the customizations happen in the Particle generator.

You pass a particle generator function to the Emitter, and it will call
the generator whenever it needs a new one. Consumers don't
create particles; they just pass in a generator. The Emitter
tracks its particles in a Linked List, which is more efficient
when dealing with large numbers of objects that have variable
life spans, and therefore can be deleted at any time. I may 
move deleted particles into an Object Pool for better memory
management.

The Emitter itself has a location within its canvas. Most of our
visualizations ignore it, however. It's useful for mouse-tracking
(having an emitter spit out particles from the mouse location)
or otherwise moving the point from which particles are emitted.
I mention this because all of the drawing routines take parameters
indicating the emitter's position, and they are always ignored.

## Particle

Most particles are fairly simple when taken individually. The Particle
class has lots of options for tweaking Particles: how fast are
they moving? how long do they live? what color are they?

You can (and often will) override the core draw() function.
Once your particle is set up (with position, size, opacity, etc.),
your draw routine will do all the work. 

A few basic drawing routines are provided: drawing a circle, or
a circle with soft edges, drawing an image. If you want to see
an interesting example of a custom draw routine, see the "flyballs"
visualization. 