
// function to install the spokes in the wheel
//

var DomReady = require('../tools/dom-ready');

DomReady(function () {

    var i = 1, ct = 12,
        s = '';

    for (; i <= ct; ++i)
        s += '<div class="spoke spoke-' + i + '"></div>';

    document.getElementById('wheel')
        .insertAdjacentHTML('beforeend', s);
});
